<?php

namespace AppBundle\Security;

use AppBundle\Entity\CasoClinico;
use AppBundle\Entity\CasoClinicoRepository;
use AppBundle\Entity\ResultadoExamenRepository;
use AppBundle\Entity\Usuario;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

/**
 * @Service("kern.exam_voter", public=false)
 * @Tag("security.voter")
 */
class ExamenVoter implements VoterInterface
{
    const EXAMINARSE = 'examinarse';

    /**
     * @var CasoClinicoRepository
     */
    private $casoClinicoRepo;

    /**
     * @var ResultadoExamenRepository
     */
    private $resultadoExamenRepo;

    /**
     * @InjectParams({
     *     "casoClinicoRepo"     = @Inject("kern.repo.caso_clinico"),
     *     "resultadoExamenRepo" = @Inject("kern.repo.resultado_examen")
     * })
     */
    public function __construct(CasoClinicoRepository $casoClinicoRepo, ResultadoExamenRepository $resultadoExamenRepo)
    {
        $this->casoClinicoRepo     = $casoClinicoRepo;
        $this->resultadoExamenRepo = $resultadoExamenRepo;
    }

    /**
     * @inheritdoc
     */
    public function supportsClass($class)
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function supportsAttribute($attribute)
    {
        return self::EXAMINARSE === $attribute;
    }

    /**
     * @inheritdoc
     */
    public function vote(TokenInterface $token, $object, array $attributes)
    {
        foreach ($attributes as $attribute) {
            if (!$this->supportsAttribute($attribute)) {
                return self::ACCESS_ABSTAIN;
            }
        }

        $user = $token->getUser();

        if (!$user instanceof Usuario) {
            return self::ACCESS_DENIED;
        }

        $examenHecho = $this->resultadoExamenRepo
            ->findOneBy(['examinante' => $user]);

        if ($examenHecho) {
            return self::ACCESS_DENIED;
        }

        $casoClinico = $this->casoClinicoRepo
            ->findOneBy(['autor' => $user]);

        if (!$casoClinico || CasoClinico::ESTADO_ENTREGADO !== $casoClinico->getEstado()) {
            return self::ACCESS_DENIED;
        }

        if ($casoClinico->getCreatedAt() < new \DateTime('-10 days')) {
            return self::ACCESS_DENIED;
        }

        return self::ACCESS_GRANTED;
    }
}
