<?php

namespace AppBundle\Listener;

use AppBundle\Service\UserRouter;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Service;
use JMS\DiExtraBundle\Annotation\Tag;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * @Service("kern.listener")
 * @Tag("kernel.event_listener", attributes = {"event" = "security.interactive_login"})
 * @Tag("kernel.event_listener", attributes = {"event" = "kernel.response"})
 */
class LoginListener
{
    /**
     * @var UserRouter
     */
    private $userRouter;

    /**
     * @var string|null
     */
    private $securedAreaUrl;

    /**
     * @InjectParams({
     *     "userRouter" = @Inject("kern.user_router")
     * })
     */
    public function __construct(UserRouter $userRouter)
    {
        $this->userRouter = $userRouter;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $this->securedAreaUrl = $this->userRouter
            ->getSecuredAreaUrl();
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if ($this->securedAreaUrl) {
            $event->setResponse(new RedirectResponse($this->securedAreaUrl));
        }
    }
}
