<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\CasoClinico;
use AppBundle\Entity\DeferredEmail;
use AppBundle\Entity\Favorito;
use AppBundle\Entity\ResultadoExamen;
use AppBundle\Entity\Usuario;
use AppBundle\Entity\Voto;
use AppBundle\Service\ExamEvaluator;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * Loads an Administrator user to the database
 */
class FixturesLoader implements FixtureInterface
{
    /**
     * @var Generator
     */
    private $faker;

    /**
     * @var PasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        $this->faker = Factory::create('es_ES');
        $this->encoder = new BCryptPasswordEncoder(4);
    }

    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $casosCompletos = $this->configurarResidentes(
            $this->createUsuarios(2000, 'residente', Usuario::ROLE_RESIDENTE)
        );

        $this->configurarEvaluadores(
            $this->createUsuarios(200, 'andrologo', Usuario::ROLE_ANDROLOGO),
            $casosCompletos
        );

        $this->configurarEvaluadores(
            $this->createUsuarios(10, 'jurado', Usuario::ROLE_JURADO),
            $casosCompletos
        );

        $this->createUsuarios(5, 'colaborador', Usuario::ROLE_COLABORADOR);
        $this->createUsuarios(1, 'admin', Usuario::ROLE_ADMIN);
    }

    /**
     * Crea 4 grupos de residentes distintos:
     *
     *  - Residentes sin caso clinico presentado
     *  - Residentes con el caso clinico a medio redactar
     *  - Residentes con el caso clínico completado
     *  - Residentes con el caso clínico completado y examinados
     *
     * @param Usuario[] $residentes
     *
     * @return CasoClinico[] Los casos clinicos completados
     */
    private function configurarResidentes(array $residentes)
    {
        $grupos = array_chunk($residentes, count($residentes) / 4 + 1);

        foreach ($grupos[1] as $residente) {
            $this->createCasoClinico($residente, CasoClinico::ESTADO_PENDIENTE);
        }

        foreach ($grupos[2] as $residente) {
            $this->createCasoClinico($residente, CasoClinico::ESTADO_ENTREGADO);

            $email = new DeferredEmail(
                $residente->getEmail(),
                'recordatorio_examen_5dias',
                $this->faker->text(),
                $this->getDateInNextMonth()
            );

            $this->manager->persist($email);

            $email = new DeferredEmail(
                $residente->getEmail(),
                'recordatorio_examen_9dias',
                $this->faker->text(),
                $this->getDateInNextMonth()
            );

            $this->manager->persist($email);
        }

        $evaluator = new ExamEvaluator();

        $casosCompletos = [];
        foreach ($grupos[3] as $residente) {
            $casosCompletos[] = $this->createCasoClinico($residente, CasoClinico::ESTADO_COMPLETADO);

            $respuestaExamen = [];
            for ($i = 1; $i <= 10; ++$i) {
                $respuestaExamen[$i] = $this->faker->randomElement(['a', 'b', 'c', 'd', 'e']);
            }

            $resultadoExamen = new ResultadoExamen(
                $residente,
                $respuestaExamen,
                $evaluator->evaluarExamen($respuestaExamen)
            );

            $this->manager->persist($resultadoExamen);
        }

        $this->manager->flush();

        return $casosCompletos;
    }

    /**
     * Cada usuario vota y marca como favorito un numero aleatorio de casos clínicos
     *
     * @param Usuario[]     $evaluadores
     * @param CasoClinico[] $casos
     */
    private function configurarEvaluadores(array $evaluadores, array $casos)
    {
        foreach ($evaluadores as $evaluador) {
            $faker = Factory::create();
            $votos = mt_rand(0, 2);
            for ($i = 0; $i < $votos; ++$i) {
                $voto = new Voto($evaluador, $faker->unique()->randomElement($casos));
                $this->manager->persist($voto);
            }

            $faker = Factory::create();
            $favs = mt_rand(0, count($casos));
            for ($i = 0; $i < $favs; ++$i) {
                $favorito = new Favorito($evaluador, $faker->unique()->randomElement($casos));
                $this->manager->persist($favorito);
            }
        }
    }

    /**
     * @return Usuario[]
     */
    private function createUsuarios($amount, $password, $primaryRole)
    {
        $usuarios = [];

        for ($i = 0; $i < $amount; ++$i) {
            $usuario = new Usuario();
            $usuario
                ->setEmail($this->faker->unique()->email)
                ->setPassword($this->encoder->encodePassword($password, null))
                ->setNombre($this->faker->firstName)
                ->setApellidos($this->faker->lastName)
                ->setMovil($this->faker->phoneNumber)
                ->setCiudad($this->faker->city)
                ->setHospital($this->faker->address)
                ->addRole($primaryRole)
                ->setCreatedAt($this->faker->dateTimeThisYear);

            $this->manager->persist($usuario);

            $usuarios[] = $usuario;
        }

        $this->manager->flush();

        return $usuarios;
    }

    /**
     * @param  Usuario     $autor
     * @param  integer     $estado
     * @return CasoClinico
     */
    private function createCasoClinico(Usuario $autor, $estado)
    {
        $createdAt = clone $autor->getCreatedAt();
        $createdAt->add(new \DateInterval('P1D'));

        $casoClinico = new CasoClinico();
        $casoClinico
            ->setAutor($autor)
            ->setEstado($estado)
            ->setTitulo($this->faker->sentence())
            ->setDescripcion($this->faker->sentence())
            ->setAutores($this->faker->name)
            ->setHospital($this->faker->address)
            ->setEdad($this->faker->randomNumber())
            ->setClinica($this->faker->address)
            ->setExploraciones($this->faker->text())
            ->setDiagnostico($this->faker->text())
            ->setTratamiento($this->faker->text())
            ->setComplicaciones($this->faker->text())
            ->setAspectosRelevantes($this->faker->text())
            ->setBibliografia($this->faker->text())
            ->setCreatedAt($createdAt);

        $this->manager->persist($casoClinico);

        return $casoClinico;
    }

    /**
     * @return \DateTime
     */
    private function getDateInNextMonth()
    {
        $now = new \DateTime();
        $nextMonth = new \DateTime('+1 month');

        return new \DateTime('@'.mt_rand($now->getTimestamp(), $nextMonth->getTimestamp()));
    }
}
