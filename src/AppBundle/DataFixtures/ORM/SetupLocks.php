<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Lock;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Sets up mandatory Lock entities in DB
 */
class SetupLocks implements FixtureInterface
{
    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        $manager->persist(new Lock('registro'));
        $manager->persist(new Lock('votacion_publica'));
        $manager->persist(new Lock('votacion_privada'));

        $manager->flush();
    }
}
