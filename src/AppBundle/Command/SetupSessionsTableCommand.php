<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This command leverages the PdoSessionHandler::createTable()
 * method from the session.handler.pdo service.
 */
class SetupSessionsTableCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('setup:sessions')
            ->setDescription('Creates the sessions table');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sessionHandler = $this->getContainer()->get('session.handler.pdo');

        try {
            $sessionHandler->createTable();
        } catch (\Exception $e) {
            $output->writeln("<error>Exception thrown while attempting to create the 'sessions' table</error>");
            $output->writeln($e->getMessage());
            $output->writeln($e->getTraceAsString());

            return -1;
        }

        $output->writeln("<info>'sessions' table created successfully</info>");

        return 0;
    }
}
