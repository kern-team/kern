<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendDeferredEmailsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('send:emails')
            ->setDescription('Sends matured deferred emails');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container   = $this->getContainer();
        $doctrine    = $container->get('doctrine');
        $emailSender = $container->get('kern.email_sender');
        $logger      = $container->get('logger');

        $em = $doctrine->getManager();
        $repo = $doctrine->getRepository('AppBundle:DeferredEmail');

        foreach ($repo->getMatureDeferredEmails() as $email) {
            $sent = $emailSender->sendEmail(
                $email->getToAddress(),
                $email->getSubject(),
                $email->getBody()
            );

            if ($sent) {
                $em->remove($email);
            } else {
                $logger->critical("DeferredEmail '{$email->getId()}' could not be sent!");
            }
        }

        $em->flush();

        return 0;
    }
}
