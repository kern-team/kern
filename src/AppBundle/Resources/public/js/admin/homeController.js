window.adminApp.controller('homeController', ['$scope', '$http',
    function ($scope, $http) {

        $scope.model = {
            active: 'residente',
            rankingList: []
        };

        $scope.getRangkingList = function () {
            $http.get( Routing.generate('admin_data', {offset: 0, mode: 'residentes'}))
            .success(function (response) {
                var res = JSON.parse(response);
                
                $scope.model.rankingList = res.residentes;
            })
        };

        $scope.getRangkingList();
    }
]);
