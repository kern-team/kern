window.adminApp.controller('workController', ['$scope', '$http', 
	function ($scope, $http) {
		$scope.close = function (lock) {

			$http.post(
				Routing.generate('toggle_lock', {lock: lock}),
				{'active': true}
			).success( function (response) {
				
				if (response.status) {
					alert('Se ha Cerrado: ' + lock + ' correctamente');
				} else {
					alert('Se ha abierto: ' + lock + ' correctamente');
				}
			})
		}
	}
])
