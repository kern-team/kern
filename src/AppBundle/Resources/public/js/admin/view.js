window.adminApp.controller('viewController', ['$scope',  '$http',
    function ($scope, $http) {
    	$scope.model = {};

    	$http.get(Routing.generate('view_data'))
    	.then(function (response) {
    		$scope.model.logedUsers = JSON.parse(response.data.users);

    		console.log($scope.model.casos);
    	});

    	$scope.getCase = function (userId) {
    		$http.get(Routing.generate('get_case', {userId: userId}))
	    	.then(function (response) {
	    		$scope.model.caso = JSON.parse(response.data.caso);

	    		if($scope.model.caso instanceof Array) {
	    			$scope.model.caso = $scope.model.caso[0];
	    		}

	    		console.log($scope.model.caso);
	    	});

    	}
    }
]);
