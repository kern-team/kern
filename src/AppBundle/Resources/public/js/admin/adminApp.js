window.adminApp = angular.module('adminApp',['ngRoute'])
.controller('adminController', ['$scope', 
    function ($scope) {

    }
]).config(function($routeProvider) {
    $routeProvider
    .when('/home', {
        templateUrl: 'home.html',
        controller: 'homeController'
    })
    .when('/staflist', {
        templateUrl: 'staflist.html',
        controller: 'staflistController'
    })
    .when('/work', {
        templateUrl: 'work.html',
        controller: 'workController'
    })
    .when('/view', {
        templateUrl: 'view.html',
        controller: 'viewController'
    })
    .otherwise({
        templateUrl: 'home.html',
        controller: 'homeController'
    });
});




