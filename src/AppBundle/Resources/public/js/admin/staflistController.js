window.adminApp.controller('staflistController',['$scope', '$http',
    function ($scope, $http) {
        var allCasesLoaded = false,
            offsetCase = 0;

        $scope.model = {
            active: 'residentes',
            loadingData: false,
            rankingList: []
        };

        $scope.viewTable = function (table) {
            $scope.model.active      = table;
            $scope.model.rankingList = [];
            offsetCase     = 0;
            allCasesLoaded = false;
            getCases(table);
        };

        function getCases (active) {
            $scope.model.loadingData = true;

            $http.get( Routing.generate('admin_data', {offset: offsetCase, mode: active}))
            .success(function (result) {
                var res = JSON.parse(result);

                for (response in res){
                    for (index in res[response]) {
                        $scope.model.rankingList.push(res[response][index]);
                    }

                    if (res[response].length < 10) allCasesLoaded = true
                }


                $scope.model.loadingData = false;

                offsetCase += 10

            }).error(function (error) {
            });
        }

        $(document).scroll(function (evn) {
            if ($(document).height() - ($(document).scrollTop() + $(window).height()) <= 0 && !allCasesLoaded  && !$scope.model.loadingData) {
                getCases($scope.model.active);
            }

        });

        getCases($scope.model.active);
    }
]);
