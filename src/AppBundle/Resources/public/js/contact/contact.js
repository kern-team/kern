(function (window, document, angular) {
    window.appContact = angular.module('contactApp', []);

    window.appContact.controller('contactController', ['$scope', '$http', function ($scope, $http) {
        $scope.validateParameters = function (evn) {
            if (!checkFields(['name', 'surname', 'email', 'message']) ){
                $scope.model.sending = true;

                $http.post(Routing.generate('send_contact'), $scope.model)
                .success(function (response) {
                    $scope.model.sending = false
                    $("#modal-contact").modal('show');
                }).error( function (error) {
                    $scope.model.sending = false
                });
            }
        };

        $scope.goHome = function () {
            $("#modal-contact").modal('hide');
            window.location = Routing.generate('homepage');
        }

        function checkFields (fields) {
            var invalidField = false;

            for(field in fields) {
                if ($scope.model[fields[field]] == '' || $scope.model[fields[field]] == undefined) {
                    $scope.success[fields[field]] = false;
                    $scope.error[fields[field]]   = true;

                    invalidField = true;
                } else {
                    $scope.error[fields[field]]   = false;
                    $scope.success[fields[field]] = true;
                }
            }
            return invalidField;
        }

        $scope.model   = {
            sending: false
        };
        $scope.error   = {};
        $scope.success = {};
    }])

})(window, document, angular);
