window.residentApp.controller('caseController', ['$scope', '$timeout', '$http', function ($scope, $timeout, $http) {
    var modal = $('#caseCompleted');

    $scope.error = {};
    $scope.success = {};
    $scope.model = {
       caso: $scope.$parent.model.caso,
       caseSended: $scope.$parent.model.caseSended,
       acceptTerms: false
    };

    $scope.save = function (evnt) {
        evnt.preventDefault();

        if (!checkFields(['titulo', 'descripcion', 'autores', 'hospital', 'edad', 'clinica', 'exploraciones', 'diagnostico', 'tratamiento', 'complicaciones', 'aspectos', 'bibliografia', 'terms'])) {
            $scope.success = {};
            $scope.model.caso.finalizado = true;
            sendData()
        }
    };

    $scope.saveDraft = function (evnt) {
        evnt.preventDefault();
        $scope.model.caso.finalizado = false;
        sendData(true);
    };

    $scope.goTest = function () {
        modal.modal('hide');
        $scope.$parent.model.testDisabled = false;
        $scope.$parent.model.include = 'test.html';
    }

    sendData = function (isDraft) {
        $scope.model.saving = true;
        $http.post(Routing.generate('residente_home'), $scope.model.caso)
        .success(function (result) {
            $scope.$parent.testDisabled = false;
            $scope.$parent.model.caso   = $scope.model.caso;
            
            if (!isDraft) {
                $scope.model.caseSended = true;
                $scope.$parent.model.caseSended = true;

                modal.modal('show');
            } else {
                $scope.model.saving = false;
                $('#draftSaved').modal('show');
            }
        }).error(function (error, code) {
            $scope.model.saving = false;
        });
    };

    function checkFields (fields) {
        var invalidField = false;

        for(field in fields) {
            if ($scope.model.caso[fields[field]] == '' || $scope.model.caso[fields[field]] == undefined) {
                $scope.success[fields[field]] = false;
                $scope.error[fields[field]] = true;
                invalidField = true;
            } else {
                $scope.error[fields[field]] = false;
                $scope.success[fields[field]] = true;
            }
        }
        return invalidField;
    }

}]);

