window.residentApp.controller('testController', ['$scope', '$http', 
    function ($scope, $http) {
        $scope.model = {
            sendingTest: false,
            test: {
                1: undefined, 2: undefined, 3: undefined, 4: undefined, 5: undefined, 6: undefined, 7: undefined, 8: undefined, 9: undefined, 10: undefined
            }
        };

        $scope.isComplet = function () {
            for(value in $scope.model.test){
                if ($scope.model.test[value] == undefined) return true;
            }

            return false;
        }

        $scope.goCase = function () {
            $scope.$parent.model.testDisabled = true;
            $scope.$parent.model.include = 'case.html';
            $('#testScore').modal('hide');
        }

        $scope.sendTest = function (evn) {
            $http.post(Routing.generate('evaluar'), $scope.model.test)
            .success(function (response) {
                $('#testScore').modal('show');
            }).error(function (error) {
            });
        }

    }
]);
