window.residentApp.controller('residentController', ['$scope', '$http',
    function($scope, $http) {
        $scope.model = {
            include: 'case.html',
            testDisabled: true,
            caso: {},
            caseSended: false
        }

        $scope.showInclude = function (view) {
            if (!$scope.model.testDisabled) {
                $scope.model.include = view;
            }
        }
    }
])
