(function (window, document, angular) {
    window.appRegister = angular.module('registerApp', []);

    window.appRegister.controller('registerController', ['$scope', function ($scope) {
        $scope.validateParameters = function (evn) {
            if (checkFields(['name', 'surname', 'email', 'phone', 'city', 'hospital', 'password']) ){
                evn.preventDefault();
            }
        };

        function checkFields (fields) {
            var invalidField = false;

            for(field in fields) {
                if ($scope.model[fields[field]] == '' || $scope.model[fields[field]] == undefined) {
                    $scope.success[fields[field]] = false;
                    $scope.error[fields[field]] = true;

                    invalidField = true;
                } else {
                    $scope.error[fields[field]] = false;
                    $scope.success[fields[field]] = true;
                }
            }
            return invalidField;
        }

        $scope.model   = {
            cargo: 'residente'
        };
        
        $scope.error   = {};
        $scope.success = {};
    }])

})(window, document, angular);
