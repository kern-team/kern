window.andrologoApp.controller('caseListController', ['$scope', '$http', 
    function($scope, $http){
        var modal = $('#voteCase'),
            favourites  = [],
            votes = [],
            allCasesLoaded = false,
            offsetCase = 0,
            orderSelected = 'date';

        $scope.model = {
            cases: [],
            modalStatus: 1,
            sending: false,
            loadingData: true
        };

        $scope.addFavorite = function (cas) {
            cas.favourite = !cas.favourite;

            $http.post(Routing.generate('fav', {id: cas.id}),[],
                function (result) {
                }, function (error) {

                }
            );
        };

        $scope.voteCase = function (cas) {
            $scope.model.activeCase = cas;

            switch (votes.length) {
                case 0:
                    $scope.confirmVote();
                    break;
                case 1:
                    if (!cas.voted) {
                        $scope.model.modalStatus = 1
                        modal.modal('show');
                    } else {
                        if (votes.indexOf(cas.id) >= 0) votes.splice(votes.indexOf(cas.id),1);
                        $scope.confirmVote(); 
                    }
                    break;
                case 2:
                    if (!cas.voted) {
                        $scope.model.modalStatus = 2;
                        modal.modal('show'); 
                    } else {
                        if (votes.indexOf(cas.id) >= 0) votes.splice(votes.indexOf(cas.id),1);
                        $scope.confirmVote();
                    }
                    break;               
            }
        }

        $scope.showCase = function (cas) {
            $scope.$parent.model.activeCase = cas;
            $scope.$parent.model.include = 'case.html';
        }

        $scope.confirmVote = function () {
            $scope.model.sending = true;

            $http.post(Routing.generate('vote', {id: $scope.model.activeCase.id}),[])
            .success(function (result) {
                if (!$scope.model.activeCase.voted) votes.push($scope.model.activeCase.id)
                
                $scope.model.activeCase.voted = !$scope.model.activeCase.voted;
                $scope.model.activeCase       = undefined;
                $scope.model.sending          = false;
                modal.modal('hide');
            }).error(function (error, status) {
                $scope.model.activeCase = undefined;
                $scope.model.sending    = false;
                modal.modal('hide');
            });
        }

        $scope.orderBy = function (order) {
            $scope.model.cases = [];
            orderSelected      = order;
            offsetCase         = 0;
            allCasesLoaded     = false;
            getCases(orderSelected);
        }

        function getCases (orderBy) {
            $scope.model.loadingData = true;

            $http.get( Routing.generate('get_data', {offset: offsetCase, mode: orderBy}))
            .success(function (result) {
                var res = JSON.parse(result);

                for (index in res.casos) {
                    $scope.model.cases.push(res.casos[index]);
                }

                if (res.casos.length < 10) allCasesLoaded = true
                $scope.model.loadingData = false;

                favourites = res.favs;
                votes      = res.votos;
                offsetCase += 10
                
                if (votes.length >= 2) $scope.model.modalStatus = 2;

                for (index in $scope.model.cases) {
                    if (favourites.indexOf($scope.model.cases[index].id) >= 0) {
                        $scope.model.cases[index].favourite = true;
                    }

                    if (votes.indexOf($scope.model.cases[index].id) >= 0) {
                        $scope.model.cases[index].voted = true;
                    }
                }
            }).error(function (error) {
            });
        }

        $(document).scroll(function (evn) {
            if ($(document).height() - ($(document).scrollTop() + $(window).height()) <= 0 && !allCasesLoaded  && !$scope.model.loadingData) {
                getCases(orderSelected);
            }
            
        });
        getCases(orderSelected);
    }
])
