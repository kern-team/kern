window.andrologoApp.controller('caseController', ['$scope', 
    function($scope){
        $scope.model = {
            caso: $scope.$parent.model.activeCase
        }
        
        $scope.backCaseList = function () {
            $scope.$parent.model.activeCase = undefined;
            $scope.$parent.model.include = 'caseList.html';
        }
    }
])
