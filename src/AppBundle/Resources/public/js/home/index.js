(function (window, document, $){
    if (localStorage.getItem('cookieAdvise') == 'true'){
        $('#cookies').hide();
    } else {
        $('#cookies').removeClass('hide');

        $('#acceptCookies').click(function (evn) {
            $('#cookies').animate({bottom: -60});
            localStorage.setItem('cookieAdvise', true)
        });
    }
})(window, document, $)
