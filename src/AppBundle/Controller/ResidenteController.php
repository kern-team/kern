<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CasoClinico;
use AppBundle\Entity\ResultadoExamen;
use AppBundle\Entity\Usuario;
use AppBundle\Service\EmailRenderer;
use AppBundle\Service\EmailSender;
use AppBundle\Service\ExamEvaluator;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/residentes")
 * @Security("is_granted('ROLE_RESIDENTE')")
 */
class ResidenteController extends Controller
{
    /**
     * @var EmailRenderer
     *
     * @Inject("kern.email_renderer")
     */
    private $emailRenderer;

    /**
     * @var EmailSender
     *
     * @Inject("kern.email_sender")
     */
    private $emailSender;

    /**
     * @var ExamEvaluator
     *
     * @Inject("kern.exam_evaluator")
     */
    private $examEvaluator;

    /**
     * @var EntityManager
     *
     * @Inject("doctrine.orm.entity_manager")
     */
    private $manager;

    /**
     * @Method({"GET", "POST"})
     * @Route("/concurso", name="residente_home")
     */
    public function indexAction(Request $request)
    {
        /** @var Usuario $residente */
        $residente = $this->getUser();

        $casoExistente = $this->manager->getRepository('AppBundle:CasoClinico')
            ->findOneBy(['autor' => $residente]);

        $resultadoExamen = $this->manager->getRepository('AppBundle:ResultadoExamen')
            ->findOneBy(['examinante' => $residente]);

        if ('GET' === $request->getMethod()) {
            return $this->render(
                'residentes/home.html.twig',
                ['residente' => $residente, 'caso' => $casoExistente, 'resultadoExamen' => $resultadoExamen]
            );
        }

        if ($casoExistente && CasoClinico::ESTADO_PENDIENTE !== $casoExistente->getEstado()) {
            throw new AccessDeniedException('Tu caso clínico ya fue entregado');
        }

        $postData = json_decode($request->getContent(), true);

        $estado         = isset($postData['finalizado']) && $postData['finalizado'] ? CasoClinico::ESTADO_ENTREGADO : CasoClinico::ESTADO_PENDIENTE;
        $titulo         = isset($postData['titulo']) ? $postData['titulo'] : null;
        $descripcion    = isset($postData['descripcion']) ? $postData['descripcion'] : null;
        $autores        = isset($postData['autores']) ? $postData['autores'] : null;
        $hospital       = isset($postData['hospital']) ? $postData['hospital'] : null;
        $edad           = isset($postData['edad']) ? $postData['edad'] : null;
        $clinica        = isset($postData['clinica']) ? $postData['clinica'] : null;
        $exploraciones  = isset($postData['exploraciones']) ? $postData['exploraciones'] : null;
        $diagnostico    = isset($postData['diagnostico']) ? $postData['diagnostico'] : null;
        $tratamiento    = isset($postData['tratamiento']) ? $postData['tratamiento'] : null;
        $complicaciones = isset($postData['complicaciones']) ? $postData['complicaciones'] : null;
        $aspectos       = isset($postData['aspectos']) ? $postData['aspectos'] : null;
        $bibliografia   = isset($postData['bibliografia']) ? $postData['bibliografia'] : null;

        $caso = $casoExistente ?: new CasoClinico();
        $caso
            ->setAutor($residente)
            ->setEstado($estado)
            ->setTitulo($titulo)
            ->setDescripcion($descripcion)
            ->setAutores($autores)
            ->setHospital($hospital)
            ->setEdad($edad)
            ->setClinica($clinica)
            ->setExploraciones($exploraciones)
            ->setDiagnostico($diagnostico)
            ->setTratamiento($tratamiento)
            ->setComplicaciones($complicaciones)
            ->setAspectosRelevantes($aspectos)
            ->setBibliografia($bibliografia)
            ->setCreatedAt(new \DateTime()); // Date MUST be updated when CasoClinico already existed

        if (CasoClinico::ESTADO_ENTREGADO === $estado) {
            $body = $this->emailRenderer->renderEmailBody(
                'recordatorio_examen_5dias',
                ['nombre' => $residente->getNombre()]
            );

            $this->emailSender->deferredEmail(
                $residente->getEmail(),
                $this->emailRenderer->getEmailSubject('recordatorio_examen_5dias'),
                $body,
                new \DateTime('+5 days')
            );

            $body = $this->emailRenderer->renderEmailBody(
                'recordatorio_examen_9dias',
                ['nombre' => $residente->getNombre()]
            );

            $this->emailSender->deferredEmail(
                $residente->getEmail(),
                $this->emailRenderer->getEmailSubject('recordatorio_examen_9dias'),
                $body,
                new \DateTime('+9 days')
            );
        }

        $this->manager->persist($caso);
        $this->manager->flush();

        return new Response(null, Response::HTTP_CREATED);
    }

    /**
     * @Method("POST")
     * @Route("/evaluar", name="evaluar")
     * @Security("is_granted('examinarse')")
     */
    public function evaluarExamenAction(Request $request)
    {
        $examen = json_decode($request->getContent(), true);

        if (null === $examen && JSON_ERROR_NONE !== json_last_error()) {
            throw new BadRequestHttpException('Formato de respuestas inválido');
        }

        if (10 !== count($examen)) {
            throw new BadRequestHttpException('Formato de respuestas inválido');
        }

        foreach ($examen as $pregunta => $respuesta) {
            if (intval($pregunta) < 1 || intval($pregunta) > 10 || $respuesta < 'a' || $respuesta > 'e') {
                throw new BadRequestHttpException('Formato de respuestas inválido');
            }
        }

        /** @var Usuario $residente */
        $residente = $this->getUser();

        $this->emailSender
            ->abortDeferredEmail(
                $residente->getEmail(),
                $this->emailRenderer->getEmailSubject('recordatorio_examen_5dias')
            );

        $this->emailSender
            ->abortDeferredEmail(
                $residente->getEmail(),
                $this->emailRenderer->getEmailSubject('recordatorio_examen_9dias')
            );

        $casoClinico = $this->getDoctrine()->getRepository('AppBundle:CasoClinico')
            ->findOneBy(['autor' => $residente]);

        $casoClinico
            ->setEstado(CasoClinico::ESTADO_COMPLETADO);

        $notaFinal = $this->examEvaluator
            ->evaluarExamen($examen);

        $resultado = new ResultadoExamen($residente, $examen, $notaFinal);

        $this->manager->persist($resultado);
        $this->manager->flush();

        return new Response($notaFinal, Response::HTTP_CREATED);
    }
}
