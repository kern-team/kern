<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CasoClinico;
use AppBundle\Entity\CasoClinicoRepository;
use AppBundle\Entity\Favorito;
use AppBundle\Entity\Usuario;
use AppBundle\Entity\Voto;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/andrologos")
 * @Security("is_granted('ROLE_COLABORADOR')")
 */
class AndrologoController extends Controller
{
    /**
     * @Method("GET")
     * @Route("/review", name="andrologo_home")
     */
    public function indexAction()
    {
        return $this->render('andrologos/home.html.twig', ['andrologo' => $this->getUser()]);
    }

    /**
     * @Method("GET")
     * @Route("/data", name="get_data")
     */
    public function retrieveDataAction(Request $request)
    {
        $usuario = $this->getUser();
        $doctrine = $this->getDoctrine();

        $offset = intval($request->query->get('offset', 0));
        $mode = $request->query->get('mode', CasoClinicoRepository::MODE_DATE);

        if (
            $mode !== CasoClinicoRepository::MODE_DATE &&
            $mode !== CasoClinicoRepository::MODE_TITLE &&
            $mode !== CasoClinicoRepository::MODE_FAV
        ) {
            throw new BadRequestHttpException('Unrecognized mode');
        }

        $casosClinicos = $doctrine->getRepository('AppBundle:CasoClinico')
            ->retrieveCasos($offset, $mode, $this->getUser());

        $votos = $doctrine->getRepository('AppBundle:Voto')
            ->getCasosIdFromUsuario($usuario);

        $favoritos = $doctrine->getRepository('AppBundle:Favorito')
            ->getCasosIdFromUsuario($usuario);

        $mergedData = [
            'casos' => $casosClinicos,
            'votos' => $votos,
            'favs'  => $favoritos,
        ];

        return new JsonResponse($this->get('jms_serializer')->serialize($mergedData, 'json'));
    }

    /**
     * @Method("POST")
     * @Route("/vote/{id}", name="vote")
     * @Security("is_granted('ROLE_ANDROLOGO')")
     */
    public function voteAction($id)
    {
        /** @var Usuario $votante */
        $votante = $this->getUser();
        $doctrine = $this->getDoctrine();

        $lockVotacion = $doctrine->getRepository('AppBundle:Lock')
            ->find($votante->hasRole(Usuario::ROLE_JURADO) ? 'votacion_privada' : 'votacion_publica');

        if ($lockVotacion->isActive()) {
            throw new AccessDeniedException('La votación está cerrada');
        }

        $casoClinico = $doctrine->getRepository('AppBundle:CasoClinico')
            ->find($id);

        if (!$casoClinico) {
            throw new NotFoundHttpException('Este caso clínico no existe');
        }

        $votos = $doctrine->getRepository('AppBundle:Voto')
            ->findBy(['votante' => $votante]);

        $manager = $doctrine->getManager();

        foreach ($votos as $voto) {
            if ($casoClinico === $voto->getCaso()) {
                $manager->remove($voto);
                $manager->flush();

                return new Response(null, Response::HTTP_OK);
            }
        }

        if (count($votos) >= 2) {
            throw new AccessDeniedException('Ya has usado todos tus votos');
        }

        $voto = new Voto($votante, $casoClinico);

        $manager->persist($voto);
        $manager->flush();

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @Method("POST")
     * @Route("/fav/{id}", name="fav")
     */
    public function favAction($id)
    {
        $votante = $this->getUser();
        $doctrine = $this->getDoctrine();
        $manager = $doctrine->getManager();
        $casoClinico = $doctrine->getRepository('AppBundle:CasoClinico')
            ->find($id);

        if (!$casoClinico) {
            throw new NotFoundHttpException('Este caso clínico no existe');
        }

        $favYaEmitido = $doctrine->getRepository('AppBundle:Favorito')
            ->findOneBy(['votante' => $votante, 'caso' => $casoClinico]);

        if ($favYaEmitido) {
            $manager->remove($favYaEmitido);
            $manager->flush();

            return new Response(null, Response::HTTP_OK);
        }

        $favorito = new Favorito($votante, $casoClinico);

        $manager->persist($favorito);
        $manager->flush();

        return new Response(null, Response::HTTP_OK);
    }
}
