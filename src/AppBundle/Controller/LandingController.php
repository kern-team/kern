<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Usuario;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Security("is_granted('IS_AUTHENTICATED_ANONYMOUSLY')")
 */
class LandingController extends Controller
{
    const ANDROLOGO_PASSCODE = 'E7BA9730';

    /**
     * @Method("GET")
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        if ($redirectResponse = $this->authenticatedUserRedirect()) {
            return $redirectResponse;
        }

        $authUtils = $this->get('security.authentication_utils');

        return $this->render('login/index.html.twig', [
            'last_username' => $authUtils->getLastUsername(),
            'error'         => $authUtils->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Method("GET")
     * @Route("/contact", name="contact")
     */
    public function contactAction()
    {
        return $this->render('contact/index.html.twig', []);
    }

    /**
     * @Method("POST")
     * @Route("/contact", name="send_contact")
     */
    public function sendContact(Request $request)
    {
        $name = $request->request->get('name');
        $surname = $request->request->get('surname');
        $email = $request->request->get('email');
        $message = $name.' '.$surname.' pregunta:<br><br>'.$request->request->get('message');

        $this->get('kern.email_sender')
            ->sendEmail($this->container->getParameter('support.email'), 'Nueva consulta recibida', $message, $email);

        return new Response(null, 200);
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/registro", name="registro")
     */
    public function registroAction(Request $request)
    {
        if ($redirectResponse = $this->authenticatedUserRedirect()) {
            return $redirectResponse;
        }

        if ('GET' === $request->getMethod()) {
            $authUtils = $this->get('security.authentication_utils');

            return $this->render('registro/index.html.twig', [
                'last_username'  => $authUtils->getLastUsername(),
                'error'          => $authUtils->getLastAuthenticationError(),
                'last_nombre'    => '',
                'last_apellidos' => '',
                'last_movil'     => '',
                'last_ciudad'    => '',
                'last_hospital'  => '',
                'last_cargo'     => '',
                'last_password'  => ''
            ]);
        }

        $nombre    = $request->request->get('nombre', '');
        $apellidos = $request->request->get('apellidos', '');
        $email     = $request->request->get('email', '');
        $movil     = $request->request->get('movil', '');
        $ciudad    = $request->request->get('ciudad', '');
        $hospital  = $request->request->get('hospital', '');
        $cargo     = $request->request->get('cargo', '');
        $password  = $request->request->get('password', '');

        $doctrine = $this->getDoctrine();

        $lockRegistro = $doctrine->getRepository('AppBundle:Lock')
            ->find('registro');

        if ($lockRegistro->isActive()) {
            throw new AccessDeniedException('El registro está cerrado');
        }

        $usuarioExistente = $doctrine->getRepository('AppBundle:Usuario')
            ->findOneBy(['email' => $email]);

        if ($usuarioExistente) {
            return $this->render(
                'registro/index.html.twig', [
                    'last_username'  => '',
                    'error'          => 'email_taken',
                    'last_nombre'    => $nombre,
                    'last_apellidos' => $apellidos,
                    'last_movil'     => $movil,
                    'last_ciudad'    => $ciudad,
                    'last_hospital'  => $hospital,
                    'last_cargo'     => $cargo,
                    'last_password'  => $password
                ]
            );
        }

        if ('andrologo' === $cargo) {
            $passCode = $request->request->get('codigo');

            if (!$passCode || self::ANDROLOGO_PASSCODE !== $passCode) {
                return $this->render(
                    'registro/index.html.twig', [
                        'last_username'  => '',
                        'error'          => 'invalid_code',
                        'last_nombre'    => $nombre,
                        'last_apellidos' => $apellidos,
                        'last_movil'     => $movil,
                        'last_ciudad'    => $ciudad,
                        'last_hospital'  => $hospital,
                        'last_cargo'     => $cargo,
                        'last_password'  => $password
                    ]
                );
            }
        }

        $passwordEncoder = $this->get('security.password_encoder');

        $usuario = new Usuario();
        $usuario
            ->setEmail($email)
            ->setPassword($passwordEncoder->encodePassword($usuario, $password))
            ->setNombre($nombre)
            ->setApellidos($apellidos)
            ->setMovil($movil)
            ->setCiudad($ciudad)
            ->setHospital($hospital);

        switch ($cargo) {
            case 'residente':
                $usuario->addRole(Usuario::ROLE_RESIDENTE);
                break;
            case 'andrologo':
                $usuario->addRole(Usuario::ROLE_ANDROLOGO);
                break;
            default:
                throw new BadRequestHttpException('Cargo no válido');
                break;
        }

        $errors = $this->get('validator')
            ->validate($usuario);

        if (count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }

        $manager = $doctrine->getManager();
        $manager->persist($usuario);
        $manager->flush();

        // This authenticates the user automagically
        $this->get('security.token_storage')
            ->setToken(new UsernamePasswordToken($usuario, null, 'default', $usuario->getRoles()));

        return $this->redirectToRoute('homepage');
    }

    /**
     * Returns a RedirectResponse to the appropriate URL when the current
     * user is already logged in.
     *
     * @return RedirectResponse|null
     */
    private function authenticatedUserRedirect()
    {
        $userHomeUrl = $this->get('kern.user_router')
            ->getSecuredAreaUrl();

        return !is_null($userHomeUrl) ?
            $this->redirect($userHomeUrl) :
            null;
    }
}
