<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Usuario;
use AppBundle\Entity\UsuarioRepository;
use AppBundle\Service\EmailRenderer;
use AppBundle\Service\EmailSender;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\ValidatorInterface;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class AdminController extends Controller
{
    /**
     * @var EmailRenderer
     *
     * @Inject("kern.email_renderer")
     */
    private $emailRenderer;

    /**
     * @var EmailSender
     *
     * @Inject("kern.email_sender")
     */
    private $emailSender;

    /**
     * @var EntityManager
     *
     * @Inject("doctrine.orm.entity_manager")
     */
    private $manager;

    /**
     * @var UserPasswordEncoderInterface
     *
     * @Inject("security.password_encoder")
     */
    private $passwordEncoder;

    /**
     * @var UsuarioRepository
     *
     * @Inject("kern.repo.usuario")
     */
    private $usuarioRepository;

    /**
     * @var ValidatorInterface
     *
     * @Inject("validator")
     */
    private $validator;

    /**
     * @Method("GET")
     * @Route("/panel", name="admin_home")
     */
    public function indexAction()
    {
        $userStats = $this->usuarioRepository
            ->getUserStats();

        return $this->render('admin/index.html.twig', $userStats);
    }

    /**
     * @Method("GET")
     * @Route("/data", name="admin_data")
     */
    public function getRankingData(Request $request)
    {
        $offset = intval($request->query->get('offset', 0));
        $mode = $request->query->get('mode', UsuarioRepository::MODE_RESIDENTES);

        if (
            $mode !== UsuarioRepository::MODE_RESIDENTES &&
            $mode !== UsuarioRepository::MODE_ANDROLOGOS &&
            $mode !== UsuarioRepository::MODE_JURADO &&
            $mode !== UsuarioRepository::MODE_COLABORADORES
        ) {
            throw new BadRequestHttpException('Unrecognized mode');
        }

        $data = $this->usuarioRepository
            ->retrieveRankingData($offset, $mode);

        return new JsonResponse($this->get('jms_serializer')->serialize($data, 'json'));
    }

        /**
     * @Method("GET")
     * @Route("/view-data", name="view_data")
     */
    public function getViewData(Request $request)
    {
        $mode = $request->query->get('mode', UsuarioRepository::MODE_RESIDENTES);

        if (
            $mode !== UsuarioRepository::MODE_RESIDENTES &&
            $mode !== UsuarioRepository::MODE_ANDROLOGOS &&
            $mode !== UsuarioRepository::MODE_JURADO &&
            $mode !== UsuarioRepository::MODE_COLABORADORES
        ) {
            throw new BadRequestHttpException('Unrecognized mode');
        }

        $users = $this->usuarioRepository
            ->getUser();


        $response = [];

        $response['users'] = $this->get('jms_serializer')->serialize($users, 'json');


        return new JsonResponse($response);
    }

            /**
     * @Method("GET")
     * @Route("/view-case/{userId}", name="get_case")
     */
    public function getCase(Request $request, $userId)
    {
        $mode = $request->query->get('mode', UsuarioRepository::MODE_RESIDENTES);

        if (
            $mode !== UsuarioRepository::MODE_RESIDENTES &&
            $mode !== UsuarioRepository::MODE_ANDROLOGOS &&
            $mode !== UsuarioRepository::MODE_JURADO &&
            $mode !== UsuarioRepository::MODE_COLABORADORES
        ) {
            throw new BadRequestHttpException('Unrecognized mode');
        }

        $casos = $this->usuarioRepository
            ->getCasosClinicos($userId);

        $response = [];

        $response['caso'] = $this->get('jms_serializer')->serialize($casos, 'json');


        return new JsonResponse($response);
    }


    /**
     * @Method("POST")
     * @Route("/toggle/{lock}", name="toggle_lock")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function toggleLockAction($lock)
    {
        $lock = $this->getDoctrine()
            ->getRepository('AppBundle:Lock')
            ->find($lock);

        if (!$lock) {
            throw new NotFoundHttpException('lock id not found');
        }

        $lock->setActive(!$lock->isActive());

        $this->getDoctrine()
            ->getManager()
            ->flush();

        return new JsonResponse(['status' => $lock->isActive()], Response::HTTP_OK);
    }

    /**
     * @Method("POST")
     * @Route("/colaborador", name="nuevo_colaborador")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function nuevoColaborador(Request $request)
    {
        $email = $request->request->get('email');

        return $this->nuevaAlta($email, Usuario::ROLE_COLABORADOR);
    }

    /**
     * @Method("POST")
     * @Route("/juez", name="nuevo_juez")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function nuevoJuez(Request $request)
    {
        $email = $request->request->get('email');

        return $this->nuevaAlta($email, Usuario::ROLE_JURADO);
    }

    private function nuevaAlta($email, $tipo)
    {
        $usuarioExistente = $this->usuarioRepository
            ->findOneBy(['email' => $email]);

        if ($usuarioExistente) {
            throw new AccessDeniedException("Ya existe un usuario con email $email");
        }

        $stats = $this->usuarioRepository
            ->getUserStats()[$tipo === Usuario::ROLE_COLABORADOR ? 'colaboradores' : 'jurado'];

        $nombre = $tipo === Usuario::ROLE_COLABORADOR ?
            'Colaborador' :
            'Juez';

        $password = substr(hash('sha256', uniqid(true)), 0, 6);

        $miembro = new Usuario();
        $miembro
            ->setEmail($email)
            ->addRole($tipo)
            ->setPassword($this->passwordEncoder->encodePassword($miembro, $password))
            ->setNombre("$nombre #$stats")
            ->setApellidos('')
            ->setMovil('')
            ->setCiudad('')
            ->setHospital('');

        $errors = $this->validator
            ->validate($miembro);

        if (count($errors) > 0) {
            throw new BadRequestHttpException((string) $errors);
        }

        $this->manager->persist($miembro);
        $this->manager->flush();

        $body = $this->emailRenderer
            ->renderEmailBody('alta_miembro', ['password' => $password, 'rol' => $nombre]);

        $this->emailSender
            ->sendEmail($email, $this->emailRenderer->getEmailSubject('alta_miembro'), $body);

        return new Response(null, Response::HTTP_OK);
    }
}
