<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class DeferredEmailRepository extends EntityRepository
{
    /**
     * Returns an array of DeferredEmails ready for delivery
     *
     * @return DeferredEmail[]
     */
    public function getMatureDeferredEmails()
    {
        $query = $this->_em->createQuery('
            SELECT de
            FROM AppBundle:DeferredEmail de
            WHERE de.dueDate < :now
        ')->setParameter('now', new \DateTime());

        return $query->getResult();
    }
}
