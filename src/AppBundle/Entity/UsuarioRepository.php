<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class UsuarioRepository extends EntityRepository
{
    const MODE_RESIDENTES = 'residentes';
    const MODE_ANDROLOGOS = 'andrologos';
    const MODE_JURADO = 'jurado';
    const MODE_COLABORADORES = 'colaboradores';

    public function getUserStats()
    {
        $query = $this->_em->createQuery('
            SELECT u.roles, count(u.roles)
            FROM AppBundle:Usuario u
            GROUP BY u.roles
        ');

        $cleanResult = [];

        foreach($query->getArrayResult() as $elem) {
            if (in_array(Usuario::ROLE_RESIDENTE, $elem['roles'])) {
                $cleanResult['residentes'] = intval($elem[1]);
            } else if (in_array(Usuario::ROLE_ANDROLOGO, $elem['roles'])) {
                $cleanResult['andrologos'] = intval($elem[1]);
            } else if (in_array(Usuario::ROLE_JURADO, $elem['roles'])) {
                $cleanResult['jurado'] = intval($elem[1]);
            } else if (in_array(Usuario::ROLE_COLABORADOR, $elem['roles'])) {
                $cleanResult['colaboradores'] = intval($elem[1]);
            }
        }

        return $cleanResult;
    }

    public function getUser () {
        $query = $this->_em->createQuery('
            SELECT u FROM AppBundle:Usuario u
        ');

        $residentes = $query->getResult();

        return $residentes;
    }


    public function getCasosClinicos ($userId) {
        $query = $this->_em->createQuery("
            SELECT c FROM AppBundle:casoClinico c where c.autor = $userId
        ");

        $casos = $query->getResult();

        return $casos;
    }

    public function getViewdata() {
        $query = $this->_em->createQuery('
            SELECT re, u, cc
            FROM AppBundle:ResultadoExamen re JOIN re.examinante u JOIN AppBundle:CasoClinico cc
            WHERE cc.autor = u
        ');

        $residentesData = [];
        $examenesYCasos = $query->getResult();
        for ($i = 0; $i < count($examenesYCasos); $i = $i + 2) {
            $residentesData[$examenesYCasos[$i+1]->getId()] = [
                'usuario' => $examenesYCasos[$i]->getExaminante(),
                'resultado' => $examenesYCasos[$i],
                'caso' => $examenesYCasos[$i+1],
                'nota_examen' => $examenesYCasos[$i]->getNota(),
                'nota_jurado' => 0,
                'nota_andrologos' => 0,
                'puntuacion' => 0,
                'votos' => 0
            ];
        }

        unset($examenesYCasos);

        $query = $this->_em->createQuery('
            SELECT v, cc
            FROM AppBundle:Voto v JOIN v.caso cc
            WHERE v.puntos = 5
        ');

        /** @var Voto $voto */
        foreach($query->getResult() as $voto) {
            $residentesData[$voto->getCaso()->getId()]['nota_jurado'] += 5;
        }

        $query = $this->_em->createQuery('
            SELECT v, COUNT(v) as numVotos, cc
            FROM AppBundle:Voto v JOIN v.caso cc
            WHERE v.puntos = 0 GROUP BY v.caso ORDER BY numVotos DESC
        ');

        $votosAndrologos = $query->getResult();
        for ($i = 0; $i < count($votosAndrologos); ++$i) {
            $id = $votosAndrologos[$i][0]->getCaso()->getId();
            $residentesData[$id]['votos'] = $votosAndrologos[$i]['numVotos'];

            if ($i < 5){
                $residentesData[$id]['nota_andrologos'] = 5 - $i;
            }
        }

        foreach ($residentesData as $key => $elem) {
            $residentesData[$key]['puntuacion'] = $elem['nota_examen'] + $elem['nota_jurado'] + $elem['nota_andrologos'];
        }

        usort($residentesData, function($a, $b) { return $b['puntuacion'] - $a['puntuacion']; });

        $selectedSet = array_slice($residentesData, $offset, 10);

        unset($residentesData);

        return array_map(function($residenteEntry) {
            return [
                'nombre'          => $residenteEntry['usuario']->getNombre(),
                'apellidos'       => $residenteEntry['usuario']->getApellidos(),
                'titulo_caso'     => $residenteEntry['caso']->getTitulo(),
                'fecha_caso'      => $residenteEntry['caso']->getCreatedAt(),
                'nota_examen'     => $residenteEntry['nota_examen'],
                'nota_jurado'     => $residenteEntry['nota_jurado'],
                'nota_andrologos' => $residenteEntry['nota_andrologos'],
                'puntuacion'      => $residenteEntry['puntuacion'],
                'votos'           => $residenteEntry['votos']
            ];
        }, $selectedSet);

    }

    public function retrieveRankingData($offset = 0, $mode = self::MODE_RESIDENTES)
    {
        switch ($mode) {
            case self::MODE_RESIDENTES:
                    return [$mode => $this->getResidenteData($offset)];
                break;

            case self::MODE_ANDROLOGOS:
            case self::MODE_JURADO:
            case self::MODE_COLABORADORES:
                    return [$mode => $this->getVotanteData($offset, $mode)];
                break;

            default:
                throw new \RuntimeException('Unrecognized mode');
        }
    }

    private function getResidenteData($offset)
    {
        $query = $this->_em->createQuery('
            SELECT re, u, cc
            FROM AppBundle:ResultadoExamen re JOIN re.examinante u JOIN AppBundle:CasoClinico cc
            WHERE cc.autor = u
        ');

        $residentesData = [];
        $examenesYCasos = $query->getResult();
        for ($i = 0; $i < count($examenesYCasos); $i = $i + 2) {
            $residentesData[$examenesYCasos[$i+1]->getId()] = [
                'usuario' => $examenesYCasos[$i]->getExaminante(),
                'resultado' => $examenesYCasos[$i],
                'caso' => $examenesYCasos[$i+1],
                'nota_examen' => $examenesYCasos[$i]->getNota(),
                'nota_jurado' => 0,
                'nota_andrologos' => 0,
                'puntuacion' => 0,
                'votos' => 0
            ];
        }

        unset($examenesYCasos);

        $query = $this->_em->createQuery('
            SELECT v, cc
            FROM AppBundle:Voto v JOIN v.caso cc
            WHERE v.puntos = 5
        ');

        /** @var Voto $voto */
        foreach($query->getResult() as $voto) {
            $residentesData[$voto->getCaso()->getId()]['nota_jurado'] += 5;
        }

        $query = $this->_em->createQuery('
            SELECT v, COUNT(v) as numVotos, cc
            FROM AppBundle:Voto v JOIN v.caso cc
            WHERE v.puntos = 0 GROUP BY v.caso ORDER BY numVotos DESC
        ');

        $votosAndrologos = $query->getResult();
        for ($i = 0; $i < count($votosAndrologos); ++$i) {
            $id = $votosAndrologos[$i][0]->getCaso()->getId();
            $residentesData[$id]['votos'] = $votosAndrologos[$i]['numVotos'];

            if ($i < 5){
                $residentesData[$id]['nota_andrologos'] = 5 - $i;
            }
        }

        foreach ($residentesData as $key => $elem) {
            $residentesData[$key]['puntuacion'] = $elem['nota_examen'] + $elem['nota_jurado'] + $elem['nota_andrologos'];
        }

        usort($residentesData, function($a, $b) { return $b['puntuacion'] - $a['puntuacion']; });

        $selectedSet = array_slice($residentesData, $offset, 10);

        unset($residentesData);

        return array_map(function($residenteEntry) {
            return [
                'nombre'          => $residenteEntry['usuario']->getNombre(),
                'apellidos'       => $residenteEntry['usuario']->getApellidos(),
                'titulo_caso'     => $residenteEntry['caso']->getTitulo(),
                'fecha_caso'      => $residenteEntry['caso']->getCreatedAt(),
                'nota_examen'     => $residenteEntry['nota_examen'],
                'nota_jurado'     => $residenteEntry['nota_jurado'],
                'nota_andrologos' => $residenteEntry['nota_andrologos'],
                'puntuacion'      => $residenteEntry['puntuacion'],
                'votos'           => $residenteEntry['votos']
            ];
        }, $selectedSet);
    }

    private function getVotanteData($offset, $mode)
    {
        switch ($mode) {
            case self::MODE_ANDROLOGOS:
                $role = '%'.Usuario::ROLE_ANDROLOGO.'%';
                break;
            case self::MODE_JURADO:
                $role = '%'.Usuario::ROLE_JURADO.'%';
                break;
            case self::MODE_COLABORADORES:
            default:
                $role = '%'.Usuario::ROLE_COLABORADOR.'%';
                break;
        }

        $query = $this->_em->createQuery('
            SELECT u
            FROM AppBundle:Usuario u
            WHERE u.roles LIKE :role ORDER BY u.createdAt DESC
        ')
            ->setFirstResult($offset)
            ->setMaxResults(10)
            ->setParameter('role', $role);

        $cleanResult = [];
        $rawResult = $query->getResult();
        foreach ($rawResult as $votante) {
            $cleanResult[$votante->getId()] = [
                'nombre' => $votante->getNombre(),
                'apellidos' => $votante->getApellidos(),
                'fecha_registro' => $votante->getCreatedAt(),
                'titulos' => []
            ];
        }

        $query = $this->_em->createQuery('
            SELECT v, c
            FROM AppBundle:Voto v JOIN v.caso c
            WHERE v.votante IN (:votantes)
        ')->setParameter('votantes', $rawResult);

        $votos = $query->getResult();
        foreach($votos as $voto) {
            $cleanResult[$voto->getVotante()->getId()]['titulos'][] = $voto->getCaso()->getTitulo();
        }

        $finalResult = [];
        foreach ($cleanResult as $elem) {
            $finalResult[] = $elem;
        }

        return $finalResult;
    }
}

