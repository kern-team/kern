<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="votos")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\VotoRepository")
 */
class Voto
{
    /**
     * @var Usuario
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Usuario")
     */
    private $votante;

    /**
     * @var CasoClinico
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="CasoClinico")
     */
    private $caso;

    /**
     * @var integer
     *
     * @ORM\Column(name="puntos", type="integer")
     */
    private $puntos;

    /**
     * @param Usuario     $usuario
     * @param CasoClinico $casoClinico
     */
    public function __construct(Usuario $usuario, CasoClinico $casoClinico)
    {
        // Los votos del jurado valen 5 puntos, los de los andrólogos 0
        $puntos = $usuario->hasRole(Usuario::ROLE_JURADO) ? 5 : 0;

        $this->votante = $usuario;
        $this->caso    = $casoClinico;
        $this->puntos  = $puntos;
    }

    /**
     * Get votante
     *
     * @return Usuario
     */
    public function getVotante()
    {
        return $this->votante;
    }

    /**
     * Get caso
     *
     * @return CasoClinico
     */
    public function getCaso()
    {
        return $this->caso;
    }

    /**
     * Get puntos
     *
     * @return integer
     */
    public function getPuntos()
    {
        return $this->puntos;
    }
}
