<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ResultadoExamen
 *
 * @ORM\Table(name="resultados_examen")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ResultadoExamenRepository")
 */
class ResultadoExamen
{
    /**
     * @var Usuario
     *
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id"), referencedColumnName="id")
     */
    private $examinante;

    /**
     * @var integer
     *
     * @ORM\Column(name="nota", type="smallint")
     */
    private $nota;

    /**
     * @var array
     *
     * @ORM\Column(name="respuestas", type="json_array")
     */
    private $respuestas;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    public function __construct(Usuario $examinante, array $respuestas, $nota)
    {
        $this->examinante = $examinante;
        $this->respuestas = $respuestas;
        $this->nota       = $nota;
        $this->createdAt  = new \DateTime();
    }

    /**
     * Get examinante
     *
     * @return Usuario
     */
    public function getExaminante()
    {
        return $this->examinante;
    }

    /**
     * Get nota
     *
     * @return integer
     */
    public function getNota()
    {
        return $this->nota;
    }

    /**
     * Get respuestas
     *
     * @return array
     */
    public function getRespuestas()
    {
        return $this->respuestas;
    }
}
