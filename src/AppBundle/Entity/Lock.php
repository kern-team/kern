<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ResultadoExamen
 *
 * @ORM\Table(name="locks")
 * @ORM\Entity()
 */
class Lock
{
    /**
     * @var Usuario
     *
     * @ORM\Id
     * @ORM\Column(name="nombre", type="string")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    public function __construct($id)
    {
        $this->id     = $id;
        $this->activo = false;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Is active
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->activo;
    }

    /**
     * Set active
     *
     * @return Lock
     */
    public function setActive($activo)
    {
        $this->activo = $activo;

        return $this;
    }
}
