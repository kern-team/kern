<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class FavoritoRepository extends EntityRepository
{
    public function getCasosIdFromUsuario(Usuario $usuario)
    {
        $query = $this->_em->createQuery('
            SELECT IDENTITY(f.caso)
            FROM AppBundle:Favorito f
            WHERE f.votante = :votante
        ')->setParameter('votante', $usuario);

        $cleanResult = array_map(function ($elem) {
            return intval($elem[1]);
        }, $query->getArrayResult());

        return $cleanResult;
    }
}
