<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="favoritos")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FavoritoRepository")
 */
class Favorito
{
    /**
     * @var Usuario
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Usuario")
     */
    private $votante;

    /**
     * @var CasoClinico
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="CasoClinico")
     */
    private $caso;

    public function __construct(Usuario $usuario, CasoClinico $casoClinico)
    {
        $this->votante = $usuario;
        $this->caso = $casoClinico;
    }

    /**
     * Get votante
     *
     * @return Usuario
     */
    public function getVotante()
    {
        return $this->votante;
    }

    /**
     * Get caso
     *
     * @return CasoClinico
     */
    public function getCaso()
    {
        return $this->caso;
    }
}
