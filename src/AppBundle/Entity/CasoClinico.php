<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="casos_clinicos")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CasoClinicoRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class CasoClinico
{
    /** El Caso está a medio redactar */
    const ESTADO_PENDIENTE  = 0;

    /** El caso ya fué completado */
    const ESTADO_ENTREGADO  = 1;

    /** Además de entregado, el residente también se ha examinado */
    const ESTADO_COMPLETADO = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Serializer\Expose
     */
    private $id;

    /**
     * @var Usuario
     *
     * @ORM\OneToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(name="usuario_id"), referencedColumnName="id")
     */
    private $autor;

    /**
     * @var integer
     *
     * @ORM\Column(name="estado", type="integer")
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="autores", type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     */
    private $autores;

    /**
     * @var string
     *
     * @ORM\Column(name="hospital", type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     */
    private $hospital;

    /**
     * @var string
     *
     * @ORM\Column(name="edad", type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     */
    private $edad;

    /**
     * @var string
     *
     * @ORM\Column(name="clinica", type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     */
    private $clinica;

    /**
     * @var string
     *
     * @ORM\Column(name="exploraciones", type="text", nullable=true)
     *
     * @Serializer\Expose
     */
    private $exploraciones;

    /**
     * @var string
     *
     * @ORM\Column(name="diagnostico", type="text", nullable=true)
     *
     * @Serializer\Expose
     */
    private $diagnostico;

    /**
     * @var string
     *
     * @ORM\Column(name="tratamiento", type="text", nullable=true)
     *
     * @Serializer\Expose
     */
    private $tratamiento;

    /**
     * @var string
     *
     * @ORM\Column(name="complicaciones", type="text", nullable=true)
     *
     * @Serializer\Expose
     */
    private $complicaciones;

    /**
     * @var string
     *
     * @ORM\Column(name="aspectos_relevantes", type="text", nullable=true)
     *
     * @Serializer\Expose
     */
    private $aspectosRelevantes;

    /**
     * @var string
     *
     * @ORM\Column(name="bibliografia", type="text", nullable=true)
     *
     * @Serializer\Expose
     */
    private $bibliografia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Serializer\Expose
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set autor
     *
     * @param  Usuario     $autor
     * @return CasoClinico
     */
    public function setAutor(Usuario $autor = null)
    {
        $this->autor = $autor;

        return $this;
    }

    /**
     * Get autor
     *
     * @return Usuario
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * Set estado
     *
     * @param  integer     $estado
     * @return CasoClinico
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return integer
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set titulo
     *
     * @param  string      $titulo
     * @return CasoClinico
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set descripcion
     *
     * @param  string      $descripcion
     * @return CasoClinico
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set autores
     *
     * @param  string      $autores
     * @return CasoClinico
     */
    public function setAutores($autores)
    {
        $this->autores = $autores;

        return $this;
    }

    /**
     * Get autores
     *
     * @return string
     */
    public function getAutores()
    {
        return $this->autores;
    }

    /**
     * Set hospital
     *
     * @param  string      $hospital
     * @return CasoClinico
     */
    public function setHospital($hospital)
    {
        $this->hospital = $hospital;

        return $this;
    }

    /**
     * Get hospital
     *
     * @return string
     */
    public function getHospital()
    {
        return $this->hospital;
    }

    /**
     * Set edad
     *
     * @param  string      $edad
     * @return CasoClinico
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;

        return $this;
    }

    /**
     * Get edad
     *
     * @return string
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * Set clinica
     *
     * @param  string      $clinica
     * @return CasoClinico
     */
    public function setClinica($clinica)
    {
        $this->clinica = $clinica;

        return $this;
    }

    /**
     * Get clinica
     *
     * @return string
     */
    public function getClinica()
    {
        return $this->clinica;
    }

    /**
     * Set exploraciones
     *
     * @param  string      $exploraciones
     * @return CasoClinico
     */
    public function setExploraciones($exploraciones)
    {
        $this->exploraciones = $exploraciones;

        return $this;
    }

    /**
     * Get exploraciones
     *
     * @return string
     */
    public function getExploraciones()
    {
        return $this->exploraciones;
    }

    /**
     * Set diagnostico
     *
     * @param  string      $diagnostico
     * @return CasoClinico
     */
    public function setDiagnostico($diagnostico)
    {
        $this->diagnostico = $diagnostico;

        return $this;
    }

    /**
     * Get diagnostico
     *
     * @return string
     */
    public function getDiagnostico()
    {
        return $this->diagnostico;
    }

    /**
     * Set tratamiento
     *
     * @param  string      $tratamiento
     * @return CasoClinico
     */
    public function setTratamiento($tratamiento)
    {
        $this->tratamiento = $tratamiento;

        return $this;
    }

    /**
     * Get tratamiento
     *
     * @return string
     */
    public function getTratamiento()
    {
        return $this->tratamiento;
    }

    /**
     * Set complicaciones
     *
     * @param  string      $complicaciones
     * @return CasoClinico
     */
    public function setComplicaciones($complicaciones)
    {
        $this->complicaciones = $complicaciones;

        return $this;
    }

    /**
     * Get complicaciones
     *
     * @return string
     */
    public function getComplicaciones()
    {
        return $this->complicaciones;
    }

    /**
     * Set aspectosRelevantes
     *
     * @param  string      $aspectosRelevantes
     * @return CasoClinico
     */
    public function setAspectosRelevantes($aspectosRelevantes)
    {
        $this->aspectosRelevantes = $aspectosRelevantes;

        return $this;
    }

    /**
     * Get aspectosRelevantes
     *
     * @return string
     */
    public function getAspectosRelevantes()
    {
        return $this->aspectosRelevantes;
    }

    /**
     * Set bibliografia
     *
     * @param  string      $bibliografia
     * @return CasoClinico
     */
    public function setBibliografia($bibliografia)
    {
        $this->bibliografia = $bibliografia;

        return $this;
    }

    /**
     * Get bibliografia
     *
     * @return string
     */
    public function getBibliografia()
    {
        return $this->bibliografia;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime $createdAt
     * @return Usuario
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
