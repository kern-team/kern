<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CasoClinicoRepository extends EntityRepository
{
    const MAX_RESULTS = 10;

    const MODE_DATE  = 'date';
    const MODE_FAV   = 'fav';
    const MODE_TITLE = 'title';

    /**
     * Returns an array of CasoClinico objects
     *
     * @param integer $offset The desired offset
     * @param string  $mode   A query MODE_*
     *
     * @throws \RuntimeException For unrecognized query modes
     *
     * @return CasoClinico[]
     */
    public function retrieveCasos($offset = 0, $mode = self::MODE_DATE, Usuario $usuario)
    {
        switch ($mode) {
            case self::MODE_DATE:
                $query = $this->_em->createQuery('
                    SELECT c, u
                    FROM AppBundle:CasoClinico c JOIN c.autor u
                    WHERE c.estado = 2
                    ORDER BY c.createdAt DESC
                ');
                break;

            case self::MODE_TITLE:
                $query = $this->_em->createQuery('
                    SELECT c, u
                    FROM AppBundle:CasoClinico c JOIN c.autor u
                    WHERE c.estado = 2
                    ORDER BY c.titulo ASC
                ');
                break;

            case self::MODE_FAV:
                return $this->modeFav($offset, $usuario);

            default:
                throw new \RuntimeException('Unrecognized mode');
        }

        $query
            ->setMaxResults(self::MAX_RESULTS)
            ->setFirstResult($offset);

        $result = $query->getResult();

        return $result;
    }

    /**
     * Devuelve primero los casos favoritos de $usuario, luego el resto
     *
     * @return CasoClinico[]
     */
    private function modeFav($offset, Usuario $usuario)
    {
        $query = $this->_em->createQuery('
            SELECT IDENTITY(f.caso)
            FROM AppBundle:Favorito f
            WHERE f.votante = :usuario
        ')
            ->setParameter('usuario', $usuario);

        $casosFavoritosIds = array_map(function ($elem) {
            return intval($elem[1]);
        }, $query->getArrayResult());

        $query = $this->_em->createQuery('
            SELECT c, u
            FROM AppBundle:CasoClinico c JOIN c.autor u
            WHERE c.estado = 2 AND c.id IN (:casos_favoritos_ids) ORDER BY c.createdAt DESC
        ')
            ->setParameter('casos_favoritos_ids', $casosFavoritosIds);

        $totalFavs = $query->getResult();

        $favs = array_slice($totalFavs, $offset, self::MAX_RESULTS);

        if (self::MAX_RESULTS === count($favs)) {
            return $favs;
        }

        $maxResults = self::MAX_RESULTS - count($favs);
        $offset = $maxResults < self::MAX_RESULTS ? 0 : $offset - count($totalFavs);

        $query = $this->_em->createQuery('
            SELECT c, u
            FROM AppBundle:CasoClinico c JOIN c.autor u
            WHERE c.estado = 2 AND c.id NOT IN (:casos_favoritos_ids) ORDER BY c.createdAt DESC
        ')
            ->setMaxResults($maxResults)
            ->setFirstResult($offset)
            ->setParameter('casos_favoritos_ids', $casosFavoritosIds);

        return array_merge($favs, $query->getResult());
    }
}
