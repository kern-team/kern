<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class VotoRepository extends EntityRepository
{
    public function getCasosIdFromUsuario(Usuario $usuario)
    {
        $query = $this->_em->createQuery('
            SELECT IDENTITY(v.caso)
            FROM AppBundle:Voto v
            WHERE v.votante = :votante
        ')->setParameter('votante', $usuario);

        $cleanResult = array_map(function ($elem) {
            return intval($elem[1]);
        }, $query->getArrayResult());

        return $cleanResult;
    }

    public function getRanking()
    {
        $query = $this->_em->createQuery('
            SELECT  v AS voto, SUM(v.puntos) AS puntos, c, u
            FROM AppBundle:Voto v JOIN v.caso c JOIN c.autor u
            GROUP BY c.id
            ORDER BY puntos DESC
        ');

        $cleanResult = array_map(function ($elem) {
            return [
                'puntos' => intval($elem['puntos']),
                'caso' => $elem['voto']->getCaso()
            ];
        }, $query->getResult());

        return $cleanResult;
    }
}
