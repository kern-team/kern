<?php

namespace AppBundle\Service;

use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Service;
use Symfony\Component\Templating\EngineInterface;

/**
 * @Service("kern.email_renderer")
 */
class EmailRenderer
{
    // TODO Replace with production tag when available
    const SUBJECT_TAG = '[Concurso Andrología]';

    /**
     * @var EngineInterface
     */
    private $twigEngine;

    /**
     * @var string[]
     */
    private $subjects;

    /**
     * @InjectParams({
     *     "twigEngine" = @Inject("templating")
     * })
     */
    public function __construct(EngineInterface $twigEngine)
    {
        $this->twigEngine = $twigEngine;
        $this->registerTemplateSubjects();
    }

    /**
     * NOTICE: Register all template subjects in this array
     */
    private function registerTemplateSubjects()
    {
        $this->subjects = [
            'alta_andrologo'            => 'Invitación como miembro del jurado',
            'alta_miembro'              => 'Invitación como miembro del jurado',
            'recordatorio_examen_5dias' => 'Te quedan 5 días para hacer el test',
            'recordatorio_examen_9dias' => 'Sólo te queda un día para hacer el test'
        ];
    }

    /**
     * Returns the subject of a given email template
     *
     * @param string $template Template name
     *
     * @return null|string The template subject, if exists
     */
    public function getEmailSubject($template)
    {
        return array_key_exists($template, $this->subjects) ?
            self::SUBJECT_TAG.' '.$this->subjects[$template] :
            null;
    }

    /**
     * Renders the body of an email template
     *
     * @param string $template   Template name
     * @param array  $parameters Template parameters
     *
     * @return string Rendered email in plain HTML
     *
     * @throws \RuntimeException If the engine cannot handle the template name
     */
    public function renderEmailBody($template, array $parameters)
    {
        return $this->twigEngine
            ->render("emails/$template.html.twig", $parameters);
    }
}
