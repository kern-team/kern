<?php

namespace AppBundle\Service;

use AppBundle\Entity\Usuario;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Service;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * @Service("kern.user_router")
 */
class UserRouter
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authChecker;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @InjectParams({
     *     "authChecker"  = @Inject("security.authorization_checker"),
     *     "urlGenerator" = @Inject("router")
     * })
     */
    public function __construct(AuthorizationCheckerInterface $authChecker, UrlGeneratorInterface $urlGenerator)
    {
        $this->authChecker  = $authChecker;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * Returns the URL that points to the authenticated user's homepage.
     * The URL depends on the user role.
     *
     * @return null|string
     */
    public function getSecuredAreaUrl()
    {
        if ($this->authChecker->isGranted(Usuario::ROLE_ADMIN)) {
            $url = $this->urlGenerator->generate('admin_home');
        } elseif ($this->authChecker->isGranted(Usuario::ROLE_RESIDENTE)) {
            $url = $this->urlGenerator->generate('residente_home');
        } elseif ($this->authChecker->isGranted(Usuario::ROLE_COLABORADOR)) {
            $url = $this->urlGenerator->generate('andrologo_home');
        } else {
            $url = null;
        }

        return $url;
    }
}
