<?php

namespace AppBundle\Service;

use JMS\DiExtraBundle\Annotation\Service;

/**
 * @Service("kern.exam_evaluator")
 */
class ExamEvaluator
{
    const SOLUCION = '{"1": "c", "2": "e", "3": "e", "4": "e", "5": "b", "6": "b", "7": "d", "8": "c", "9": "d", "10": "c"}';

    /**
     * @var array
     */
    private $solucion;

    public function __construct()
    {
        $this->solucion = json_decode(self::SOLUCION, true);
    }

    /**
     * Devuelve la nota sacada en el exámen
     *
     * @param array $examen Sus respuestas al examen
     *
     * @return int Su nota final
     */
    public function evaluarExamen(array $examen)
    {
        $notaFinal = 0;

        foreach ($examen as $pregunta => $respuesta) {
            if ($this->solucion[$pregunta] === $respuesta) {
                $notaFinal++;
            }
        }

        return $notaFinal;
    }
}
