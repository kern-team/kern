<?php

namespace AppBundle\Service;

use AppBundle\Entity\DeferredEmail;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Service;

/**
 * @Service("kern.email_sender")
 */
class EmailSender
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $infoEmail;

    /**
     * @InjectParams({
     *     "em"          = @Inject("doctrine.orm.entity_manager"),
     *     "swiftMailer" = @Inject("mailer"),
     *     "infoEmail"   = @Inject("%support.email%")
     * })
     */
    public function __construct(EntityManager $em, \Swift_Mailer $mailer, $infoEmail)
    {
        $this->em        = $em;
        $this->mailer    = $mailer;
        $this->infoEmail = $infoEmail;
    }

    /**
     * Sends an email straight away
     *
     * @param string $to      Destination email address
     * @param string $subject Email subject
     * @param string $body    Email body
     * @param string $from    Origin address
     *
     * @return bool True means success
     */
    public function sendEmail($to, $subject, $body, $from = null)
    {
        $message = \Swift_Message::newInstance()
            ->setTo($to)
            ->setFrom($from ?: $this->infoEmail)
            ->setSubject($subject)
            ->setBody($body, 'text/html');

        return 1 === $this->mailer->send($message);
    }

    /**
     * Sends an email at a later time
     *
     * @param string    $to      Destination email address
     * @param string    $subject Email subject
     * @param string    $body    Email body
     * @param \DateTime $dueDate The email will be sent past this date
     */
    public function deferredEmail($to, $subject, $body, \DateTime $dueDate)
    {
        $deferredEmail = new DeferredEmail($to, $subject, $body, $dueDate);

        $this->em->persist($deferredEmail);
    }

    /**
     * Searches for scheduled DeferredEmails with the given subject and
     * destination address, and discards them.
     *
     * @param string $to
     * @param string $subject
     */
    public function abortDeferredEmail($to, $subject)
    {
        $repo = $this->em->getRepository('AppBundle:DeferredEmail');

        $emails = $repo->findBy(
            ['toAddress' => $to, 'subject' => $subject]
        );

        foreach ($emails as $email) {
            $this->em->remove($email);
        }
    }
}
